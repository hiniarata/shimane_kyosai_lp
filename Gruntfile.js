module.exports = function(grunt) {
  //グラントタスクの設定
  grunt.initConfig({
    //compassの設定
    compass: {
      //dev: {
      dist: {
        options: {
          config: 'sass/config.rb'
        }
      }
    },
    //watchの設定
    watch: {
      scss: {
        files: ['sass/*.scss'],
        tasks: ['compass']
      }
    }
  });
  //プラグインの読み込み
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');
};
