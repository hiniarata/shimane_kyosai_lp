/*
*
* @copyright    Copyright (c) hiniarata co.ltd
* @link         https://hiniarata.jp
*/

/*
* System Requirements: jQuery-1.11.2
*/
$(function() {

  /**
   * object-fit ※IE,Edge対応
   **/
  objectFitImages('img.imgObjectFitCover');
  objectFitImages('img.imgObjectFitContain');
  objectFitImages('img.imgObjectFitScaleDown');

  /**
   * ヘッダー固定
   **/
   var header = $('#is-headerFixed'); // fixed DOM
   var adclass = 'scrolled'; // add css class
   var scrollY = 15; // scroll
   $(window).scroll(function() {
     if ($(window).scrollTop() > scrollY) {
       header.addClass(adclass);
     } else {
       header.removeClass(adclass);
     }
   });


  /**
   * モバイルメニュー
   **/
  // $('.drawer').drawer();
  // $('.drawer-toggle').click(function() {
  //   if(!($('body.drawer-open .is-menuOpen-overlay').length)){
  //     $('header').append('<span class="is-menuOpen-overlay"></span>');
  //   };
  //   if($('body.drawer-close .is-menuOpen-overlay').length){
  //     $('header .is-menuOpen-overlay').remove();
  //   }
  // });


  /**
   * モバイルメニュー_アコーディオン
   **/
  $(".accordion").each(function () {
    var $parent = $(this);
    if ($(this).find(".accordionMenu--mobile").first().is(":visible")) {
      $(this).addClass("open");
    } else {
      $(this).addClass("close");
    }
    $parent.find(".trigger").first().click(function () {
      $parent.toggleClass("open").toggleClass("close").find(".accordionMenu--mobile").first().slideToggle();
      return false;
    });
    $parent.find(".accordion-close").click(function () {
      $parent.find(".trigger").first().trigger("click");
      return false;
    });
  });


  /**
   * スムーススクロール
   **/
   //URLのハッシュ値を取得
   var urlHash = location.hash;
   //ハッシュ値があればページ内スクロール
   if(urlHash) {
     //スクロールを0に戻す
     $('body,html').stop().scrollTop(0);
     setTimeout(function () {
       //ロード時の処理を待ち、時間差でスクロール実行
       scrollToAnker(urlHash) ;
     }, 100);
   }

   //通常のクリック時
   $('a[href^="#"]').click(function() {
   //ページ内リンク先を取得
   var href= $(this).attr("href");
    //リンク先が#か空だったらhtmlに
   var hash = href == "#" || href == "" ? 'html' : href;
   //スクロール実行
   scrollToAnker(hash);
     //リンク無効化
     return false;
   });

   // 関数：スムーススクロール
   // 指定したアンカー(#ID)へアニメーションでスクロール
   function scrollToAnker(hash) {
     var target = $(hash);
     var position = target.offset().top - 106;
      $('body,html').stop().animate({scrollTop:position}, 800);
    }


   /**
     * ページトップへ
     * 戻ボタン
     **/
    $("#pageTop").hide();
    $(window).on("scroll", function () {
      if ($(this).scrollTop() > 100) {
        $("#pageTop").fadeIn("fast");
      } else {
        $("#pageTop").fadeOut("fast");
      }
      var scrollHeight = $(document).height(); 
      var scrollPosition = $(window).height() + $(window).scrollTop(); 
      var footHeight = $("footer").innerHeight();
      if ( scrollHeight - scrollPosition  <= footHeight ) {
        $("#pageTop").css({
            "position":"absolute", 
            "bottom": footHeight + 12
        });
      } else { //それ以外の場合は
        $("#pageTop").css({
            "position":"fixed",
            "bottom": "70px"
        });
      }
    });
    $('#pageTop').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 400);
      return false;
    });

  /**
   * matchHeight
   * 高さを揃える
   **/
  $('.matchHeight').matchHeight();


  /**
   * image
   * 画像を半分にする
   **/
  $(window).load(function(){
    var img = '.imgHalf img'; //selector
    $(img).each(function() {
      var w = $(this).width() / 2;
      $(this).parent().css('max-width', w);
      $(this).css('width', '100%');
    });
  });


  /**
   * サムネイル表示をする
   **/
  /*
  $(".thumbnail").on("load",function(){
    var iw, ih;
    var cw = 215;
    var ch = 215;
    iw = ($(this).width() - cw) / 2;
    ih = ($(this).height() - ch) / 2;
    $(this).css("top", "-"+ih+"px");
    $(this).css("left", "-"+iw+"px");
  });
  */
  jQuery.event.add(window, "load", function () {
    var sl = '.trimming img'; //リサイズしたい画像のクラス名
    var fw = 215; // サムネイルの幅
    var fh = 215; // サムネイルの高さ
    var iw, ih;
    $(sl).each(function () {
      var w = $(this).width(); // 画像の幅(原寸)
      var h = $(this).height(); // 画像の高さ(原寸)

      //横長の画像の場合
      if (w >= h) {
        iw = (fh / h * w - fw) / 2
        $(this).height(fh); //高さをサムネイルに合わせる
        $(this).css("top", 0);
        $(this).css("left", "-" + iw + "px"); //画像のセンター合わせ
      }

      //縦長の画像の場合
      else {
        ih = (fw / w * h - fh) / 2
        $(this).width(fw); //幅をサムネイルに合わせる
        $(this).css("top", "-" + ih + "px"); //画像のセンター合わせ
        $(this).css("left", 0);
      }
    });
  });


  /**
   * フォーム_ラジオボタンをspanで包む
   **/
  $('.mod-form-radio').next("label").addClass('radioLabel');


  /**
   * フォーム_セレクトボックスをspnaで包む
   **/
  $( '.mod-form-select' ).wrap( '<span class="selectBox"></span>' );
  

//end of jQuery
});
